=== WooCommerce FirstData Societe Generale Soge Pay Gateway ===

A payment gateway for Societe Generale. A merchant account, merchant key and merchant ID are required for this gateway to function.

== Note ==

An SSL certificate is recommended for additional safety and security for your customers.