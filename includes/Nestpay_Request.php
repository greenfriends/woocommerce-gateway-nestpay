<?php

/**
 * Class RequestParams.
 * Class represents parameters required in order to communicate with payment gateway.
 *
 */
class Nestpay_Request
{
    /**
     * @var \Nestpay_Config
     */
    private $config;

    /**
     * Constructor method.
     * Must receive config object with required parameters.
     *
     * @param $config
     *
     */
    public function __construct(\Nestpay_Config $config)
    {
        $this->config = $config;
    }

    /**
     * Generate form which needs to be POSTed to gateway.
     *
     * @param string $context select type of user performing the action. one of: 'user' | 'admin'
     * @return string
     */
    public function generateForm($context = 'user')
    {
        return '<form action="' . esc_url($this->config->getEndPoint()) . '" method="post" id="nestpay_payment_form">' .
            $this->config->getMandatoryParams() .
            $this->generateButtonsAndJs() .
            '</form>';
    }

    private function generateButtonsAndJs()
    {
        $buttonTxt = __('Pay via Intesa NestPay', 'woocommerce-gateway-nestpay');
        $userMessageTxt = __('Hvala Vam na narudžbini. Preusmeravamo vas na Intesa Nestpay gde možete izvršiti plaćanje.', 'woocommerce-gateway-nestpay');
        $cancelOrderTxt = __('Otkaži narudžbinu &amp; obnovi korpu', 'woocommerce-gateway-nestpay');

        return '<input type="submit" class="button-alt" id="submit_nestpay_payment_form" value="' . $buttonTxt . '" /> 
				<a class="button cancel" href="' . $this->config->getCancelUrl() . '">' . $cancelOrderTxt . '</a>
				<script type="text/javascript">
					jQuery(function(){
						jQuery("body").block(
							{
								message: "' . $userMessageTxt . '",
								overlayCSS:
								{
									background: "#fff",
									opacity: 0.6
								},
								css: {
									padding:        20,
									textAlign:      "center",
									color:          "#555",
									border:         "3px solid #aaa",
									backgroundColor:"#fff",
									cursor:         "wait"
								}
							});
						jQuery( "#submit_nestpay_payment_form" ).click();
					});
				</script>';
    }
}
