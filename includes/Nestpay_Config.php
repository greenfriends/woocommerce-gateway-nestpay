<?php

class Nestpay_Config


{
    /**
     * @var WC_Gateway_Nestpay
     */
    private $nestPay;
    const PAYMENT_MODEL = "3d_pay_hosting";

    /** @link https://en.wikipedia.org/wiki/ISO_4217#Active_codes */
    const PAYMENT_CURRENCY = 941;

    const TRANSACTION_TYPE = 'PreAuth';

    private $clientId;
    private $storeKey;
    private $endpoint;

    private $amount;
    private $rnd;
    private $successUrl;
    private $cancelUrl;
    private $oid;

    private $mandatoryParams;

    public function __construct(WC_Order $order, $environment = 'test')
    {
        $this->nestPay = new WC_Gateway_Nestpay();
        //Set params
        // Production params
        if ($environment === 'production') {
            $this->storeKey = $this->nestPay->get_option('storeKey');
            $this->clientId = $this->nestPay->get_option('clientId');
            $this->endpoint = $this->nestPay->get_option('endpoint');
        }
        // If the it is a test environment, use the test params
        if ($environment !== 'production') {
            $this->storeKey = $this->nestPay->get_option('storeKeyTest');
            $this->clientId = $this->nestPay->get_option('clientIdTest');
            $this->endpoint = $this->nestPay->get_option('endpointTest');
        }

        $this->amount = number_format($order->get_total(), 2, ',', '');
        $this->rnd = substr(microtime(), 0, 20);
        $this->successUrl =  $order->get_checkout_order_received_url();
        $this->oid = $order->get_id();
        $this->storeKey = str_replace("|", "\\|", str_replace("\\", "\\\\", $this->storeKey));
        //@TODO test this out
        $this->cancelUrl = str_replace('&', '%26', $order->get_cancel_order_url());

        $this->processData();
    }


    /**
     * Creates form params for communication with Nestpay gateway.
     */
    public function getMandatoryParams()
    {
        $form = '' . PHP_EOL;

        foreach ($this->mandatoryParams as $key => $param) {
            $form .= '<input type="hidden" name="' . $key . '" value="' . $param . '"/>' . PHP_EOL;
        }

        return $form;
    }

    /**
     * Compiles hash parameter required for sending along with other parameters.
     *
     * @return string
     */
    private function compileHash()
    {
        return base64_encode(pack('H*', hash('sha512', $this->compileParamsToString())));
    }

    /**
     * Compiles all parameters to string for hash creation.
     *
     * @return string
     */
    private function compileParamsToString()
    {
        return $this->clientId . '|' . $this->oid . '|' . $this->amount . '|' . $this->successUrl . '|' . $this->cancelUrl
            . '|' . self::TRANSACTION_TYPE . '||' . $this->rnd . '||||' . self::PAYMENT_CURRENCY . '|' . $this->storeKey;
    }

    /**
     * Entry point for preparing data to be sent together with user to gateway.
     *
     * @return void
     */
    protected function processData()
    {
        $this->mandatoryParams['clientid'] = $this->clientId;
        $this->mandatoryParams['storetype'] = self::PAYMENT_MODEL;
        $this->mandatoryParams['TranType'] = self::TRANSACTION_TYPE;
        $this->mandatoryParams['currency'] = self::PAYMENT_CURRENCY;
        $this->mandatoryParams['hash'] = $this->compileHash();
        $this->mandatoryParams['oid'] = $this->oid;
        $this->mandatoryParams['amount'] = $this->amount;
        $this->mandatoryParams['okurl'] = $this->successUrl;
        $this->mandatoryParams['failurl'] = $this->cancelUrl;
        $this->mandatoryParams['lang'] = "sr";
        $this->mandatoryParams['refreshtime'] = 10;
        $this->mandatoryParams['encoding'] = "utf-8";
        $this->mandatoryParams['rnd'] = $this->rnd;
        $this->mandatoryParams['hashAlgorithm'] = "ver2";

        /** optional param */
        $this->mandatoryParams['orderId'] = $this->oid;
    }

    public function getEndpoint()
    {
        return $this->endpoint;
    }

    public function getCancelUrl()
    {
        return $this->cancelUrl;
    }

    public function getStoreKey()
    {
        return $this->storeKey;
    }

    public function getClientId()
    {
        return $this->clientId;
    }
}