<?php

/**
 * Class ResponseParams.
 *
 */
class Nestpay_Response
{
    /**
     * @var string
     */
    private $storeKey;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $hash;

    /**
     * @var string
     */
    private $hashVal;

    /**
     * @var array
     */
    private $response;

    /**
     * @var bool
     */
    private $isTransactionSuccessful;

    private $errorMessages;
    private $transactionId;
    private $orderId;
    private $paymentStatus;
    private $authCode;
    private $procReturnCode;
    private $transactionDate;
    private $xid;

    /**
     * ResponseParams constructor.
     *
     * @param $config
     * @param $response array response from gateway
     */
    public function __construct(\Nestpay_Config $config, $response)
    {
        $this->storeKey = $config->getStoreKey();
        $this->clientId = $config->getClientId();
        $this->response = $response;

        $this->isTransactionSuccessful = false;
        $this->processData();
        $this->setAllRequiredFields();
    }

    /**
     * Compiles hash parameter.
     */
    protected function compileHash()
    {
        $hashParams = $this->response["HASHPARAMS"];
        $hash = "";

        $parsedHashParams = explode('|', $hashParams);
        foreach ($parsedHashParams as $param) {
            $paramValue = $this->response[$param];
            if ($paramValue === "") {
                $paramValue = "";
            }
            $escapedValue = str_replace('|', '\||', str_replace('\\', '\\\\', $paramValue));
            $hash .= $escapedValue . '|';
        }

        $escapedStoreKey = str_replace('|', '\||', str_replace('\\', '\\\\', $this->storeKey));
        $hash .= $escapedStoreKey;

        $this->hashVal = $hash;
        $this->hash = base64_encode(pack('H*', hash('sha512', $hash)));
    }

    /**
     * Processing response data returned from gateway.
     *
     * @return void
     */
    protected function processData()
    {
        $requiredParameters = array('clientid', 'ReturnOid', 'Response');
        $this->errorMessages = array();
        $this->isProcessed = true;

        foreach ($requiredParameters as $parameter) {
            if ($this->response[$parameter] === null || $this->response[$parameter] === "") {
                array_push($this->errorMessages, 'Missing required param: ' . $parameter);
                $debug = 'Debug data : ' . print_r($this->response, true);
                array_push($this->errorMessages, $debug);
            }
        }

        if (count($this->errorMessages)) {
            return;
        }

        if ($this->response["clientid"] != $this->clientId) {
            array_push($this->errorMessages, 'Incorrect clientId: ' . $this->response["clientid"]);
            return;
        }

        /**
         * We use 'ver2' hash algorithm.
         * If gateway returns other version of hash algorithm, system should cancel the order.
         */
        if ($this->response['hashAlgorithm'] !== "ver2") {
            return;
        }

        $this->compileHash();

        $escapedStoreKey = str_replace('|', '\||', str_replace('\\', '\\\\', $this->storeKey));
        $hashparamsval = $this->response['HASHPARAMSVAL'] . '|' . $escapedStoreKey;

        if ($hashparamsval != $this->hashVal || $this->response['HASH'] != $this->hash) {
            array_push($this->errorMessages, "The digital signature is not valid.");
            array_push($this->errorMessages, "Generated hash: " . $this->hash);
            array_push($this->errorMessages, "Sent hash: " . $this->response['HASH']);
            return;
        }

        if (!$this->checkMdStatusFromResponse()) {
            return;
        }

        if (!$this->isApprovedTransaction()) {
            return;
        }

        $this->isTransactionSuccessful = true;
    }

    /**
     * If transaction is successful or successful, method sets all necessary fields.
     * If transaction is unsuccessful sets only orderId and paymentStatus.
     *
     * @return void
     */
    private function setAllRequiredFields()
    {
        if ($this->response['Response'] === "Declined" || !$this->isTransactionSuccessful) {
            $this->transactionId = $this->response['TransId'];
            $this->orderId = $this->response['ReturnOid'];
            $this->paymentStatus = $this->response['Response'];
            $this->xid = $this->response['xid'];
            $this->transactionDate = date("d/m/Y - H:i:s", strtotime($this->response['EXTRA_TRXDATE']));
            return;
        }

        if ($this->response['Response'] === "Approved") {
            $this->transactionId = $this->response['TransId'];
            $this->orderId = $this->response['ReturnOid'];
            $this->paymentStatus = $this->response['Response'];
            $this->authCode = $this->response['AuthCode'];
            $this->procReturnCode = $this->response['ProcReturnCode'];
            $this->xid = $this->response['xid'];
            $this->transactionDate = date("d/m/Y - H:i:s", strtotime($this->response['EXTRA_TRXDATE']));

            return;
        }

        $this->orderId = $this->response['oid'];
        $this->paymentStatus = $this->response['Response'];
    }

    /**
     * Transaction status code getter,
     *
     * @return string
     */
    public function getProcReturnCode()
    {
        return $this->procReturnCode;
    }

    /**
     * Unique Internet transaction ID getter.
     *
     * @return string
     */
    public function getXid()
    {
        return $this->xid;
    }

    /**
     * Error messages getter.
     *
     * @return array
     *
     * @throws \BadMethodCallException
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * Transaction date getter.
     *
     * @return string
     */
    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    /**
     * Checks status code for the 3D transaction.
     * For all cards that don't have '3d' type of transaction (AMEX in this case),
     * 'mdStatus' field doesn't exist in response from gateway.
     *
     * @see Nestpay documentation - Section: 4.2.1.1
     *
     * @return bool
     */
    private function checkMdStatusFromResponse()
    {
        if (!array_key_exists('mdStatus', $this->response) && $this->response['EXTRA_CARDBRAND'] === "AMEX") {
            return true;
        }

        if ($this->response['mdStatus'] === "1" ||
            $this->response['mdStatus'] === "2" ||
            $this->response['mdStatus'] === "3" ||
            $this->response['mdStatus'] === "4"
        ) {
            return true;
        }

        return false;
    }

    /**
     * Is transaction approved ? Checks field in response from gateway.
     *
     * @return bool
     */
    private function isApprovedTransaction()
    {
        if ($this->response['Response'] === "Approved" && $this->response['ProcReturnCode'] === "00") {
            return true;
        }

        return false;
    }

    /**
     * Payment status getter.
     *
     * @return string
     *
     * @throws \BadMethodCallException
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * Getter for "AuthCode" field in response.
     *
     * @return string
     *
     * @throws \BadMethodCallException
     */
    public function getAuthCode()
    {
        return $this->authCode;
    }

    /**
     * TransactionId getter.
     *
     * @return string|bool
     *
     * @throws \BadMethodCallException
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * OrderId getter.
     *
     * @return string
     *
     * @throws \BadMethodCallException
     */
    public function getOrderId()
    {
        return $this->orderId;
    }


    /**
     * Is transaction successful ?
     *
     * @return bool
     */
    public function isTransactionSuccessful()
    {
        return $this->isTransactionSuccessful;
    }

    /**
     * Is transaction declined ?
     * Return value is based on payment's status and transactionId.
     *
     * @return bool
     */
    public function isTransactionDeclined()
    {
        if ($this->response['Response'] === "Declined" && array_key_exists('TransId', $this->response)) {
            return true;
        }

        return false;
    }
}
