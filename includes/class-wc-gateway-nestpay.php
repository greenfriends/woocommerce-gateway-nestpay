<?php

//load all required classes
include_once ("Nestpay_Config.php");
include_once ("Nestpay_Request.php");
include_once ("Nestpay_Response.php");

/**
 * Nestpay Payment Gateway.
 *
 * Provides a First Data Payment Gateway integration.
 *
 * @class 		WC_Gateway_Nestpay
 * @package		WooCommerce
 * @category	Payment Gateways
 * @author		WooThemes
 */
class WC_Gateway_Nestpay extends WC_Payment_Gateway
{
    /**
     * Version
     *
     * @var string
     */
    public $version;

    /**
     * @var array $data_to_send
     */
    protected $data_to_send = array();

    protected $send_debug_email;

    protected $paymentSuccess = null;

    protected $txnType;

    public $merchant_id;
    public $merchant_key;
    public $url;
    private $environment;
    /**
     * @var mixed
     */
    private $reference;

    /**
     * Constructor method.
     */
    public function __construct() {

        $this->version = '0.1';
        $this->id = 'nestpay';
        $this->method_title = __( 'Platnom karticom', 'woocommerce-gateway-nestpay' );
        $this->title = __( 'Platnom karticom', 'woocommerce-gateway-nestpay' );
        if ($this->get_option( 'title' ) !== '') {
            $this->title = $this->get_option( 'title' );
        }
        $this->description = __( 'Platite vašom karticom, preko Banca Intesa platnog procesora.', 'woocommerce-gateway-nestpay' );
        if ($this->get_option('description') !== '') {
            $this->description = $this->get_option('description');
        }
//        $this->method_description = __( 'Platite vašom karticom, preko  Banca Intesa platnog procesora.', 'woocommerce-gateway-nestpay' );
//        $this->icon = WP_PLUGIN_URL . '/' . plugin_basename( dirname( dirname( __FILE__ ) ) ) . '/assets/images/soge.png';
        $this->debug_email = get_option( 'admin_email' );
        $this->available_currencies = array('RSD');

        // Supported functionality
        $this->supports = array(
            'products'
        );

        $this->environment = 'test';
        if ($this->get_option('testmode') === 'no') {
            $this->environment = 'production';
        }


        $this->init_form_fields();
        $this->init_settings();

        //@TODO add this option to admin, along with option to choose debug email address
        $this->send_debug_email  = 'yes' === $this->get_option('send_debug_email');


//         Setup test data, if in test mode.
//        if ($this->get_option('testmode') === 'yes') {
//            $this->add_testmode_admin_settings_notice();
//            $this->send_debug_email = true;
//        } else {
//            $this->send_debug_email = false;
//        }

        //add required info to success page
        add_action( 'woocommerce_thankyou_order_received_text', array( $this, 'beforeCart' ));
        add_action( 'woocommerce_thankyou', array( $this, 'processPayment' ));
        add_action( 'woocommerce_api_wc_gateway_nestpay', array( $this, 'checkItnResponse' ) );
        add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
        add_action( 'woocommerce_receipt_nestpay', array( $this, 'receipt_page' ) );
//        add_action( 'check_payment', array( $this, 'processPayment') );

        // Email actions
        add_action('woocommerce_email_order_details',array($this,'addEmailInformation'),9);
    }

//    public function email($data)
//    {
//        return 'email' . var_dump($data);
//    }

    // replaces the original text

    public function addEmailInformation($order)
    {
        if($order->get_payment_method() === 'nestpay') {
            $this->getCompanyInfoForEmail();
            $this->getTransactionInfoForEmail();
        }
    }
    public function beforeCart()
    {
        if ($this->paymentSuccess) {
            return __( 'Thank you for your order, your payment was successful, and your card was charged by the seller.', 'woocommerce-gateway-nestpay' );
        }
    }

    public function checkItnResponse()
    {
        mail('djavolak@mail.ru', 'test', print_r($_POST, true));
        if (isset($_POST['processor_response_code'])) {
            if ($_POST['processor_response_code'] === '00') {
                $this->processPayment();
            }
        }
    }

    public function getCompanyInfo()
    {
        $companyName = $this->get_option('companyName');
        $companyAddress = $this->get_option('companyAddress');
        $companyPib = $this->get_option('companyPib');
        $companyMb = $this->get_option('companyMb');
        include(__DIR__ . "/../templates/thxDetailsVendor.php");
    }

    public function getCompanyInfoForEmail()
    {
        $companyName = $this->get_option('companyName');
        $companyAddress = $this->get_option('companyAddress');
        $companyPib = $this->get_option('companyPib');
        $companyMb = $this->get_option('companyMb');
        include(__DIR__ . "/../templates/emails/thxDetailsVendorEmail.php");
    }

    public function getTransactionInfoForEmail()
    {
        $reference = $this->reference;
        include(__DIR__ . "/../templates/emails/thxDetailsEmail.php");
    }

    public function processPayment()
    {
        
        $this->reference = $_POST['AuthCode'];
        $this->paymentSuccess = true;
        $order = wc_get_order((int) $_POST['oid']);
        if($order) {
            //@TODO we need only orders that are in pending state
            if (strtolower($order->get_status()) == 'pending') {
                $order->payment_complete();
                $order->update_status('u-pripremiplaceno');

                //@TODO send debug mail
//            if ($this->send_debug_email) {
//                $this->sendDebugMail($order);
//            }
            }

            //if everyting ok print details
            include(__DIR__ . "/../templates/thxDetails.php");
            $this->getCompanyInfo();
        }

        return;
    }
    


    private function sendDebugMail($order)
    {
        $debug_email = $this->get_option('debug_email', get_option('admin_email'));
        $debug_email = 'djavolak@mail.ru';
        $vendor_name = get_bloginfo('name');
        $vendor_url = home_url('/');
        $subject = 'nestpay payment notification for your site';
        $body =
            "Hi,\n\n"
            . "A nestpay transaction has been completed on your website\n"
            . "------------------------------------------------------------\n"
            . 'Site: ' . $vendor_name . ' (' . $vendor_url . ")\n"
            . 'Purchase ID: ' . esc_html((int) $_POST['oid']) . "\n"
            . 'PayFast Transaction ID: ' . esc_html($_POST['ipgTransactionId']) . "\n"
            . 'PayFast Payment Status: ' . esc_html($_POST['status']) . "\n"
            . 'Order Status Code: ' . self::get_order_prop($order, 'status');
        wp_mail($debug_email, $subject, $body);
    }

    // move to response class
    private function extractResponse()
    {
        return new Nestpay_Response(new Nestpay_Config(wc_get_order($_POST['oid'])), $_POST);
    }

    /**
     * Add a notice to the merchant_key and merchant_id fields when in test mode.
     */
    public function add_testmode_admin_settings_notice() {
//        $this->form_fields['woocommerce_nestpay_testmode']['description']  .= ' <strong>' . __( 'Test Merchant ID currently in use', 'woocommerce-gateway-nestpay' ) . ' ( ' . esc_html( $this->merchant_id ) . ' ).</strong>';
//        $this->form_fields['testmode']['description'] .= ' <strong>' . __( 'Test Merchant Key currently in use', 'woocommerce-gateway-nestpay' ) . ' ( ' . esc_html( $this->merchant_key ) . ' ).</strong>';
    }

    /**
     * Generate the gateway button link.
     *
     * @return string
     */
    public function generateForm($order_id)
    {
        $order = wc_get_order($order_id);
//        $config = new Config('production');
        $config = new Nestpay_Config($order, $this->environment);
        $request = new Nestpay_Request($config);

        return $request->generateForm();
    }

    /**
     * Return payment url.
     *
     * @param int $orderId
     *
     * @return array
     */
    public function process_payment($orderId)
    {
        $order = wc_get_order($orderId);
//        $response = $this->extractResponse();
//        die('asdas');

        return array(
            'result' 	 => 'success',
            'redirect'	 => $order->get_checkout_payment_url(true),
        );
    }

    /**
     * Reciept page.
     *
     * Display text and a button to direct the user to nestpay pgw.
     */
    public function receipt_page($order)
    {
        echo '<p>' . __( 'Thank you for your order, please click the button below to pay with credit card.', 'woocommerce-gateway-nestpay' ) . '</p>';
        echo $this->generateForm($order);
    }

    /**
     * Initialise Gateway Settings Form Fields
     *
     * @return void
     */
    public function init_form_fields()
    {
        $this->form_fields = array(
//            'enabled' => array(
//                'title'       => __( 'Enable/Disable', 'woocommerce-gateway-nestpay' ),
//                'label'       => __( 'Enable Gateway', 'woocommerce-gateway-nestpay' ),
//                'type'        => 'checkbox',
//                'description' => __( 'This controls whether or not this gateway is enabled within WooCommerce.', 'woocommerce-gateway-nestpay' ),
//                'default'     => 'yes',
//                'desc_tip'    => true,
//            ),
            'title' => array(
                'title'       => __( 'Title', 'woocommerce-gateway-nestpay' ),
                'type'        => 'text',
                'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce-gateway-nestpay'),
                'default'     => __( 'NestPay', 'woocommerce-gateway-nestpay' ),
                'desc_tip'    => true,
            ),
            'description' => array(
                'title'       => __( 'Description', 'woocommerce-gateway-nestpay' ),
                'type'        => 'text',
                'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce-gateway-nestpay'),
                'default'     => '',
                'desc_tip'    => true,
            ),
            'testmode' => array(
                'title'       => __( 'Test mode', 'woocommerce-gateway-nestpay' ),
                'type'        => 'checkbox',
                'description' => __( 'Place the payment gateway in test mode.', 'woocommerce-gateway-nestpay'),
                'default'     => 'yes',
            ),
            'clientId' => array(
                'title' => __('Client ID', 'woocommerce-gateway-nestpay'),
                'type' => 'text',
                'description' => __('This is the client ID used in production', 'woocommerce-gateway-nestpay'),
                'default' => '',
                'desc_tip' => true
            ),
            'storeKey' => array(
                'title' => __('Store Key', 'woocommerce-gateway-nestpay'),
                'type' => 'text',
                'description' => __('This is the store key used in production', 'woocommerce-gateway-nestpay'),
                'default' => '',
                'desc_tip' => true
            ),
            'endpoint' => array(
                'title' => __('Endpoint', 'woocommerce-gateway-nestpay'),
                'type' => 'url',
                'description' => __('This is the endpoint used in production', 'woocommerce-gateway-nestpay'),
                'default' => '',
                'desc_tip' => true
            ),
            'clientIdTest' => array(
                'title' => __('Client ID Test', 'woocommerce-gateway-nestpay'),
                'type' => 'text',
                'description' => __('This is the client ID used in a test environment', 'woocommerce-gateway-nestpay'),
                'default' => '',
                'desc_tip' => true
            ),
            'storeKeyTest' => array(
                'title' => __('Store Key Test', 'woocommerce-gateway-nestpay'),
                'type' => 'text',
                'description' => __('This is the store key used in a test environment', 'woocommerce-gateway-nestpay'),
                'default' => '',
                'desc_tip' => true
            ),
            'endpointTest' => array(
                'title' => __('Endpoint Test', 'woocommerce-gateway-nestpay'),
                'type' => 'url',
                'description' => __('This is the endpoint used in a test environment', 'woocommerce-gateway-nestpay'),
                'default' => '',
                'desc_tip' => true
            ),
            'companyName' => array(
                    'title' => __('Company Name', 'woocommerce-gateway-nestpay'),
                    'type' => 'text',
            ),
            'companyAddress' => array(
                    'title' => __('Company Address', 'woocommerce-gateway-nestpay'),
                    'type' => 'text',
            ),
            'companyPib' => array(
                    'title' => __('Company PIB', 'woocommerce-gateway-nestpay'),
                    'type' => 'number'
            ),
            'companyMb' => array(
                'title' => __('Company MB', 'woocommerce-gateway-nestpay'),
                'type' => 'number'
            ),
        );
    }

    function woocommerce_version_check($version = '2.1') {
        if (function_exists( 'is_woocommerce_active' ) && is_woocommerce_active()) {
            global $woocommerce;
            if( version_compare( $woocommerce->version, $version, ">=" ) ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Admin Panel Options
     * - Options for bits like 'title' and availability on a country-by-country basis
     *
     */
    public function admin_options()
    {
        parent::admin_options();

        return;

        if (!$this->languageHacked) {
            if ( in_array( get_woocommerce_currency(), $this->available_currencies ) ) {
                parent::admin_options();
            } else {
                ?>
                <h3><?php _e( 'Intesa NestPay', 'woocommerce-gateway-nestpay' ); ?></h3>
                <div class="inline error"><p><strong><?php _e( 'Gateway Disabled', 'woocommerce-gateway-nestpay' ); ?></strong> <?php /* translators: 1: a href link 2: closing href */ echo sprintf( __( 'Choose South EUR or RSD as your store currency in %1$sPricing Options%2$s to enable the SoGe Pay Gateway.', 'woocommerce-gateway-nestpay' ), '<a href="' . esc_url( admin_url( 'admin.php?page=wc-settings&tab=general' ) ) . '">', '</a>' ); ?></p></div>
                <?php
            }
        } else {
            parent::admin_options();
        }
    }

    /**
     * @TODO fix this
     * Log system processes.
     */
    public function log($message)
    {
        if ($this->get_option('testmode') === 'yes') {
            if (empty($this->logger )) {
                $this->logger = new WC_Logger();
            }
            $this->logger->add('nestpay', $message);
        }
    }

    /**
     * Get order property with compatibility check on order getter introduced
     * in WC 3.0.
     *
     * @param WC_Order $order Order object.
     * @param string   $prop  Property name.
     *
     * @return mixed Property value
     */
    public static function get_order_prop($order, $prop)
    {
        switch ( $prop ) {
            case 'order_total':
                $getter = array( $order, 'get_total' );
                break;
            default:
                $getter = array( $order, 'get_' . $prop );
                break;
        }

        return is_callable( $getter ) ? call_user_func( $getter ) : $order->{ $prop };
    }
    public static function getProcessingEmailData($order)
    {
        self::getOrderDataForEmail($order);

        $html = '<h2 style="color: #e8e3d3;display: block;font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif;font-size: 18px;font-weight: bold;line-height: 130%;margin: 16px 0 8px;text-align: left;">'. __('Note', 'woocommerce') . '</h2>'
            . '<p style="margin: 0 0 16px;"><strong>'. __('All prices are shown with VAT included.', 'woocommerce-gateway-nestpay').'</strong></p>';

        $html .= '<h2 style="color: #e8e3d3;display: block;font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif;font-size: 18px;font-weight: bold;line-height: 130%;margin: 16px 0 8px;text-align: left;">'. __('Your details', 'woocommerce') . '</h2>
            <p style="margin: 0 0 16px;"><strong>'. __('Reference: ', 'woocommerce-gateway-nestpay').'</strong> '. $_POST['refnumber'] . '</p>
            <p style="margin: 0 0 16px;"><strong>'. __('Transaction ID: ', 'woocommerce-gateway-nestpay'). '</strong> '.$_POST['ipgTransactionId'] . '</p>
            <p style="margin: 0 0 16px;"><strong>'. __('Transaction time and date: ', 'woocommerce-gateway-nestpay'). '</strong> '. $_POST['txndatetime']. '</p>
            <p style="margin: 0 0 16px;"><strong>'. __('Approval code: ', 'woocommerce-gateway-nestpay'). '</strong> '.explode(':', $_POST['approval_code'])[1]. '</p>
            <p style="margin: 0 0 16px;"><strong>'. __('Status: ', 'woocommerce-gateway-nestpay'). '</strong> '.$_POST['status']. '</p>';

        echo $html;
    }

    /**
     * Return the gateway's icon.
     *
     * @return string
     */
    public function get_icon()
    {
        return '';
    }

    public static function getOrderDataForEmail($order)
    {
        $orderNo = sprintf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() );
        echo '<h2>'. $orderNo .'</h2>

<table cellspacing="0" cellpadding="6" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
    <tr>
        <th scope="col" style="text-align:left; border: 1px solid #eee;">'. __( 'Product', 'woocommerce' ) .'</th>
        <th scope="col" style="text-align:left; border: 1px solid #eee;">'. __( 'Quantity', 'woocommerce' ) .'</th>
        <th scope="col" style="text-align:left; border: 1px solid #eee;">'. __( 'Price', 'woocommerce' ) .'</th>
    </tr>';
        echo $order->email_order_items_table( true, false, true );

        if ( $totals = $order->get_order_item_totals() ) {
            $i = 0;
            foreach ( $totals as $total ) {
                $i++;
                $border = '';
                if ( $i == 1 ) {
                    $border = 'border-top-width: 4px;';
                }
                echo '<tr>
            <th scope="row" colspan="2" style="text-align:left; border: 1px solid #eee; '.$border.' ">'. $total['label'] .'</th>
            <td style="text-align:left; border: 1px solid #eee; '.$border.'">'. $total['value'] .'</td>
            </tr>';
            }
        }
        echo '</table>';
    }
}

// global assignments, added here in order do avoid messing with incompatible themes / actions / emails

//add_action('init', 'check_for_payment_actions');

//frontend
add_action('admin_enqueue_scripts', 'nestPayAssets');
function nestPayAssets() {
    wp_enqueue_style('nestpay', get_stylesheet_directory_uri() . '/../../plugins/woocommerce-gateway-nestpay/assets/css/style.css');
    wp_enqueue_script('nestpay', get_stylesheet_directory_uri() . '/../../plugins/woocommerce-gateway-nestpay/assets/js/script.js');
}

add_action('woocommerce_before_cart_table', 'beforeCartContents');
/**
 * Display required details when cancel order is detected.
 */
function beforeCartContents() {
    if (isset($_GET['cancel_order'])) {
        $orderId = '';
        foreach (explode('&amp;', $_GET['cancel_order']) as $item):
            if (explode('=', $item)[0] == 'order_id'):
                $orderId = (explode('=', $item)[1]);
            endif;
        endforeach;
        if ($_GET['cancel_order'] && $orderId !== '') {

            include(__DIR__ . "/../templates/thxDetailsFail.php");

        }
    }


    // @TODO remove cart contents
}

/**
 * set hook to activate background payment processing in order something goes wrong with user communication.
 *
 */
//function check_for_payment_actions() {
//    if (isset($_POST['hash_algorithm']) && isset($_POST['response_hash']) && isset($_GET['key'])) {
//         Start the gateways
//        WC()->payment_gateways();
//        do_action('check_payment');
//    }
//}