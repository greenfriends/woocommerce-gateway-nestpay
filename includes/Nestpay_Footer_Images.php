<?php


class Nestpay_Footer_Images
{
    public function __construct()
    {
        $this->hooksAndFilters();
    }

    public function hooksAndFilters()
    {
        add_action('wp_footer',array($this,'footerView'));
        add_action('wp_enqueue_scripts', array($this,'enqueueFooterStyle'));
    }

    public function enqueueFooterStyle()
    {
        wp_enqueue_style('nestpayFooterStyle',get_stylesheet_directory_uri() . '/../../plugins/woocommerce-gateway-nestpay/assets/css/nestpayFooter.css');
    }

    public function footerView()
    {
        echo '
       <div class="paymentImageContainer">
            <img class="visa" src="' . plugin_dir_url(__FILE__) . '../assets/images/visa.png' .'" alt="visa">
            <img class="mastercart" src="' . plugin_dir_url(__FILE__) . '../assets/images/mastercart.png' .'" alt="mastercart">
            <img class="maestro" src="' . plugin_dir_url(__FILE__) . '../assets/images/maestro.png' .'" alt="maestro">
            <img class="american" src="' . plugin_dir_url(__FILE__) . '../assets/images/americanexpress.png' .'" alt="american">
            <a class="intesaAnchor" target="_blank" href="https://www.bancaintesa.rs/pocetna.1.html" title="intesa">
                <img class="intesa" src="' . plugin_dir_url(__FILE__) . '../assets/images/intesa.png' .'" alt="intesa">
            </a>
            <a target="_blank" href="https://www.visa.ca/en_CA/run-your-business/merchant-resources/verified-by-visa.html" title="verified visa">
                <img class="visaVerified" src="' . plugin_dir_url(__FILE__) . '../assets/images/visaVerified.png' .'" alt="visa verified">
            </a>
            <a target="_blank" href="https://www.mastercard.us/en-us/business/overview.html" title="mastercard secured">
                <img class="masterSecured" src="' . plugin_dir_url(__FILE__) . '../assets/images/masterSecured.png' .'" alt="mastercard secured">
            </a>
        </div>
       ';
    }
}