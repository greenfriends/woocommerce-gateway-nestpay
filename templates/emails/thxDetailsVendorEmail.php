<h2 style="color:#006699;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:130%;margin:0 0 18px;text-align:left">
    Podaci o prodavcu:
</h2>
<p><strong><?= __('Ime: ', 'woocommerce-gateway-nestpay')?></strong><?=$companyName?></p>
<p><strong><?= __('Adresa: ', 'woocommerce-gateway-nestpay')?></strong><?=$companyAddress?></p>
<p><strong><?= __('PIB: ', 'woocommerce-gateway-nestpay')?></strong><?=$companyPib?></p>
<p><strong><?= __('MB: ', 'woocommerce-gateway-nestpay')?></strong><?=$companyMb?></p>
