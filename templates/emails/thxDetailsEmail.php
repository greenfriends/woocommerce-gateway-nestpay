<h2 style="color:#006699;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:130%;margin:0 0 18px;text-align:left">
    Detalji transakcije:
</h2>
<p><strong><?= __('Referenca: ', 'woocommerce-gateway-nestpay')?></strong><?=$reference?></p>
<p><strong><?= __('ID transakcije: ', 'woocommerce-gateway-nestpay')?></strong><?=$transId?></p>
<p><strong><?= __('Datum i vreme transakcije: ', 'woocommerce-gateway-nestpay')?></strong><?=$transDateTime?></p>
<p><strong><?= __('Kod odobrenja: ', 'woocommerce-gateway-nestpay')?></strong><?=$approvalCode?></p>
<p><strong><?= __('Status: ', 'woocommerce-gateway-nestpay')?></strong><?=$status?></p>
