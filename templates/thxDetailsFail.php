<header class="paymentTitle" style="margin-top:50px;">
    <h4><?= __('Transakcija NIJE uspela. Proverite podatke i  ', 'woocommerce-gateway-nestpay')?>
    <a href="<?=wc_get_checkout_url()?>" title="<?=__('probajte ponovo', 'woocommerce-gateway-nestpay')?>"><?=__('probajte ponovo', 'woocommerce-gateway-nestpay')?></a>.</h4>
    <h4><?= __('Detalji transakcije: ', 'woocommerce-gateway-nestpay')?></h4>
</header>

<table class="shop_table shop_table_responsive customer_details">
    <tbody>
    <tr>
        <th><?= __('Broj narudžbenice: ', 'woocommerce-gateway-nestpay')?></th>
        <td><?=$orderId?></td>
    </tr>
<!--    <tr>-->
<!--        <th>--><?php //echo __('Datum transakcije: ', 'woocommerce-gateway-nestpay')?><!--</th>-->
<!--        <td>--><?php //echo $_POST['ipgTransactionId']?><!--</td>-->
<!--    </tr>-->
    <tr>
        <th><?= __('Status transakcije: ', 'woocommerce-gateway-nestpay')?></th>
        <td><?=$_POST['txstatus']?></td>
    </tr>
    <tr>
        <th><?= __('Broj transakcije: ', 'woocommerce-gateway-nestpay')?></th>
        <td><?=$_POST['TRANID']?></td>
    </tr>
    <tr>
        <th><?= __('Jedinstveni broj internet transakcije: ', 'woocommerce-gateway-nestpay')?></th>
        <td><?=$_POST['xid']?></td>
    </tr>
    </tbody>
</table>
<?php include('thxDetailsVendor.php'); ?>

<header class="paymentTitle">
    <h4><?= __('Detalji kupca: ', 'woocommerce-gateway-nestpay')?></h4>
</header>
<table class="shop_table shop_table_responsive customer_details">
    <tbody>
    <tr>
        <th><?= __('Full name: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('Full name: ', 'woocommerce-gateway-nestpay')?>"><?=get_user_meta(get_current_user_id(), 'shipping_first_name', true)?></td>
    </tr>
    <tr>
        <th><?= __('Address: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('Address: ', 'woocommerce-gateway-nestpay')?>"><?=get_user_meta(get_current_user_id(), 'shipping_address_1', true)?></td>
    </tr>
    <tr>
        <th><?= __('Postcode: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('Postcode: ', 'woocommerce-gateway-nestpay')?>"><?=get_user_meta(get_current_user_id(), 'shipping_postcode', true)?></td>
    </tr>
    <tr>
        <th><?= __('City: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('City: ', 'woocommerce-gateway-nestpay')?>"><?=get_user_meta(get_current_user_id(), 'shipping_city', true)?></td>
    </tr>
    <tr>
        <th><?= __('Country: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('Country: ', 'woocommerce-gateway-nestpay')?>"><?=get_user_meta(get_current_user_id(), 'shipping_country', true)?></td>
    </tr>
    </tbody>
</table>
<style>
    .woocommerce-cart-form__contents, .cart-collaterals{display:none}
    .woocommerce-cart-form{color:white}
    .paymentTitle{color:black}
    .customer_details{color:black}
    .gf-seller-info{display:none}
</style>