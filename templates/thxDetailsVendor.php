<header class="paymentTitle">
    <h4><?= __('Detalji prodavca: ', 'woocommerce-gateway-nestpay')?></h4>
</header>
<table class="shop_table shop_table_responsive customer_details">
    <tbody>
    <tr>
        <th><?= __('Ime prodavca: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('Ime prodavca: ', 'woocommerce-gateway-nestpay')?>"><?=$companyName?></td>
    </tr>
    <tr>
        <th><?= __('Adresa: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('Adresa: ', 'woocommerce-gateway-nestpay')?>"><?=$companyAddress?></td>
    </tr>
    <tr>
        <th><?= __('PIB: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('PIB: ', 'woocommerce-gateway-nestpay')?>"><?=$companyPib?></td>
    </tr>
    <tr>
        <th><?= __('MB: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('MB: ', 'woocommerce-gateway-nestpay')?>"><?=$companyMb?></td>
    </tr>
    </tbody>
</table>