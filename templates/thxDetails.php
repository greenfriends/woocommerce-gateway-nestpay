<header class="title">
    <h3><?= __('Detalji transakcije: ', 'woocommerce-gateway-nestpay')?></h3>
</header>
<table class="shop_table shop_table_responsive customer_details">
    <tbody>
    <tr>
        <th><?= __('Autorizacioni kod: ', 'woocommerce-gateway-nestpay')?></th>
        <td><?=$_POST['AuthCode']?></td>
    </tr>
    <tr>
        <th><?= __('Status transakcije: ', 'woocommerce-gateway-nestpay')?></th>
        <td><?=$_POST['Response']?></td>
    </tr>
    <tr>
        <th><?= __('Kod statusa transakcije: ', 'woocommerce-gateway-nestpay')?></th>
        <td><?=$_POST['ProcReturnCode']?></td>
    </tr>
    <tr>
        <th><?= __('Broj transakcije: ', 'woocommerce-gateway-nestpay')?></th>
        <td><?=$_POST['TransId']?></td>
    </tr>
    <tr>
        <th><?= __('Statusni kod 3D transakcije: ', 'woocommerce-gateway-nestpay')?></th>
        <td><?=$_POST['mdStatus']?></td>
    </tr>
    </tbody>
</table>