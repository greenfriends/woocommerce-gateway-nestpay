<?php
/**
 * Plugin Name: WooCommerce Intesa NestPay
 * Plugin URI:
 * Description: Receive payments using the Intesa NestPay{tm} payments provider
 * Author: djavolak
 * Version: 0.0.3
 *
 * Copyright (c) 2009-2017 WooCommerce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}

/**
 * Initialize the gateway.
  */
function woocommerce_nestpay_init() {
	if ( ! class_exists( 'WC_Payment_Gateway' ) ) {
		return;
	}
	require_once( plugin_basename( 'includes/class-wc-gateway-nestpay.php' ) );
	load_plugin_textdomain( 'woocommerce-gateway-nestpay', false, trailingslashit( dirname( plugin_basename( __FILE__ ) ) ) );
	add_filter( 'woocommerce_payment_gateways', 'woocommerce_nestpay_add_gateway' );
}
add_action( 'plugins_loaded', 'woocommerce_nestpay_init', 0 );

function woocommerce_nestpay_plugin_links( $links ) {
	$settings_url = add_query_arg(
		array(
			'page' => 'wc-settings',
			'tab' => 'checkout',
			'section' => 'wc_gateway_nestpay',
		),
		admin_url( 'admin.php' )
	);

	$plugin_links = array(
		'<a href="' . esc_url( $settings_url ) . '">' . __( 'Settings', 'woocommerce-gateway-nestpay' ) . '</a>',
//		'<a href="https://support.woothemes.com/">' . __( 'Support', 'woocommerce-gateway-nestpay' ) . '</a>',
//		'<a href="https://docs.woothemes.com/document/nestpay-payment-gateway/">' . __( 'Docs', 'woocommerce-gateway-nestpay' ) . '</a>',
	);

	return array_merge( $plugin_links, $links );
}
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'woocommerce_nestpay_plugin_links' );


/**
 * Add the gateway to WooCommerce
 */
function woocommerce_nestpay_add_gateway( $methods ) {
	$methods[] = 'WC_Gateway_nestpay';
	return $methods;
}


require_once ('includes/Nestpay_Footer_Images.php');
$footerView = new Nestpay_Footer_Images();
